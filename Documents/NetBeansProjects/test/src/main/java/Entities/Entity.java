package Entities;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author IT
 */
public class Entity implements Serializable {

    private Integer codigo;
    private String compania;
    private String NoArticulo;
    private String nombreArticulo;
    private Date fechaImportacion;
    private String tipoProblema;
    private String problemas;
    private String comparacion;
    private String tecnico;
    private String totalRecibido;
    private String porcentajeProblema;
    private Date fechaEvaluacion;
    private Integer cantEvaluando;
    private Boolean estadoImportacion;
    private String commentarios;

    public Entity() {

    }

    public Entity(int codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getNoArticulo() {
        return NoArticulo;
    }

    public void setNoArticulo(String NoArticulo) {
        this.NoArticulo = NoArticulo;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public Date getFechaImportacion() {
        return fechaImportacion;
    }

    public void setFechaImportacion(Date fechaImportacion) {
        this.fechaImportacion = fechaImportacion;
    }

    public String getTipoProblema() {
        return tipoProblema;
    }

    public void setTipoProblema(String tipoProblema) {
        this.tipoProblema = tipoProblema;
    }

    public String getProblemas() {
        return problemas;
    }

    public void setProblemas(String problemas) {
        this.problemas = problemas;
    }

    public String getComparacion() {
        return comparacion;
    }

    public void setComparacion(String comparacion) {
        this.comparacion = comparacion;
    }

    public String getTecnico() {
        return tecnico;
    }

    public void setTecnico(String tecnico) {
        this.tecnico = tecnico;
    }

    public String getTotalRecibido() {
        return totalRecibido;
    }

    public void setTotalRecibido(String totalRecibido) {
        this.totalRecibido = totalRecibido;
    }

    public String getPorcentajeProblema() {
        return porcentajeProblema;
    }

    public void setPorcentajeProblema(String porcentajeProblema) {
        this.porcentajeProblema = porcentajeProblema;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public Integer getCantEvaluando() {
        return cantEvaluando;
    }

    public void setCantEvaluando(Integer cantEvaluando) {
        this.cantEvaluando = cantEvaluando;
    }

    public Boolean getEstadoImportacion() {
        return estadoImportacion;
    }

    public void setEstadoImportacion(Boolean estadoImportacion) {
        this.estadoImportacion = estadoImportacion;
    }

    public String getCommentarios() {
        return commentarios;
    }

    public void setCommentarios(String commentarios) {
        this.commentarios = commentarios;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entity other = (Entity) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codigo.toString();
    }

}
