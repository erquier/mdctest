/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import Entities.Entity;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author IT
 */
@ViewScoped
@Named
public class EntityView implements Serializable {

    private Entity element;

    @PostConstruct
    public void init() {
        element = new Entity();

        element.setCodigo(98765);
        element.setNoArticulo("Articulo");
    }

//    @PostConstruct
//    public void init() {
//        
//    }
    public void buttonAction() {
        addMessage("Welcome to Primefaces!!");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public Entity getElement() {
        return element;
    }

    public void setElement(Entity element) {
        this.element = element;
    }

}
