import Vue from 'vue'
import router from './router'
import App from './App.vue'
import axios from 'axios'
import store from './store'

//import DaySpanVuetify from 'dayspan-vuetify'


Vue.config.productionTip = false

axios.defaults.baseURL = 'https://brightgardens-67ee6.firebaseio.com/'
axios.defaults.baseURL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty'
// axios.defaults.headers.common['Authorization'] = 'fasfdsa'
axios.defaults.headers.get['Accepts'] = 'application/json'

const reqInterceptor = axios.interceptors.request.use(config => {
  console.log('Request Interceptor', config)
  return config
})
const resInterceptor = axios.interceptors.response.use(res => {
  console.log('Response Interceptor', res)
  return res
})

axios.interceptors.request.eject(reqInterceptor)
axios.interceptors.response.eject(resInterceptor)

/*Vue.use(DaySpanVuetify, {
  methods: {
    getDefaultEventColor: () => '#1976d2'
  }
});*/

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
